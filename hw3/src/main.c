#include <stdio.h>
#include <stdlib.h>

#include "hashtable.h"

#define DEFAULT_BUFFER_SIZE 32

void print(const char * word, int value) {
    printf("%s -> %d\n", word, value);
}

_Bool is_worlds_delimiter(char symbol) {
    return ' ' == symbol || '\n' == symbol || '\r' == symbol;
}

int main(int argc, char * argv[]) {
    if (argc != 2) {
        printf("Usage: %s <file>\n", argv[0]);
        return EXIT_FAILURE;
    }
    int ret_value = EXIT_SUCCESS;
    char * file_name = argv[1];
    FILE * f = fopen(file_name, "rb");
    if (NULL == f) {
        fprintf(stderr, "End: can't open file \"%s\"", file_name);
        ret_value = EXIT_FAILURE;
        goto End;
    }

    HashTable * ht = create_hash_table();
    if (NULL == ht) {
        fprintf(stderr, "End: can't create hash table");
        ret_value = EXIT_FAILURE;
        goto End;
    }

    unsigned int buffer_size = DEFAULT_BUFFER_SIZE;
    char * buffer = malloc(sizeof(unsigned char) * buffer_size);
    char current_char = '\0';
    size_t buffer_position = 0;
    while (true) {
        if (1 != fread(&current_char, sizeof(char), 1, f)) {
            if (!feof(f)) {
                fprintf(stderr, "End: can't read file \"%s\"", file_name);
                ret_value = EXIT_FAILURE;
                goto End;
            }
            break;
        }
        if (buffer_position == buffer_size) {
            buffer_size += 2;
            char * new_buffer = realloc(buffer, buffer_size);
            if (NULL == new_buffer) {
                fprintf(stderr, "End: can't realloc buffer to %d bytes", buffer_size);
                ret_value = EXIT_FAILURE;
                goto EndBuffer;
            }
            buffer = new_buffer;
        }
        if (is_worlds_delimiter(current_char)) {
            if (buffer_position > 0) {
                buffer[buffer_position] = '\0';
                hash_table_put(
                        ht,
                        buffer,
                        hash_table_exists(ht, buffer) ?
                                hash_table_get(ht, buffer) + 1 : 1
                );
            }
            buffer_position = 0;
            continue;
        }
        buffer[buffer_position++] = current_char;
    }

    hash_table_foreach_key_value(ht, print);

    EndBuffer:
    if (NULL == buffer) {
        free(buffer);
    }
    if (ht) {
        free_hash_table(ht);
    }
    End:
    if (f) {
        fclose(f);
    }
    return ret_value;
}
