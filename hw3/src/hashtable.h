#ifndef HASH_TABLE_H_
#define HASH_TABLE_H_

#include <stdbool.h>

typedef struct hashTable HashTable;

HashTable * create_hash_table(void);
HashTable * create_hash_table_with_capacity(size_t capacity);

int hash_table_put(HashTable * hash_table, const char * key, int value);
_Bool hash_table_exists(const HashTable * hash_table, const char * key);
int hash_table_get(const HashTable * hash_table, const char * key);
int hash_table_remove(const HashTable * hash_table, const char * key);

void hash_table_foreach_key_value(HashTable * hash_table, void (*fn)(const char * key, int value));

void free_hash_table(HashTable * hash_table);

#endif // HASH_TABLE_H_
