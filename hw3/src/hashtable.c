#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "hashtable.h"

typedef enum {
    empty, full, deleted
} CellType;

struct cell {
    char * key;
    int value;
    CellType cell_type;
};

typedef struct cell Cell;

struct hashTable {
    size_t capacity;
    size_t size;
    Cell ** cells;
};

static int hash(const char * str) {
    int hash = 0;
    while (*str) {
        int x = *str - 'a' + 1;
        hash = (hash * 31 + x) % INT_MAX;
        str++;
    }
    return hash;
}

static Cell ** create_hash_table_cells(size_t capacity) {
    Cell ** cells = malloc(sizeof(Cell*) * capacity);
    for (size_t i = 0; i < capacity; i++) {
        cells[i] = malloc(sizeof(Cell));
        cells[i]->key = NULL;
        cells[i]->cell_type = empty;
    }
    return cells;
}

static Cell * find_cell(Cell ** cells, size_t capacity, const char * key) {
    const int key_hash = hash(key);
    size_t bucket_number = key_hash % capacity;
    size_t not_viewed = capacity;
    while (empty != cells[bucket_number]->cell_type && not_viewed--) {
        if (!strcmp(key, cells[bucket_number]->key)) {
            return cells[bucket_number];
        }
        bucket_number++;
        bucket_number %= capacity;
    }
    return cells[bucket_number];
}

static Cell * find_cell_in_hash_table(const HashTable * hash_table, const char * key) {
    return find_cell(hash_table->cells, hash_table->capacity, key);
}

static void resize_hash_table(HashTable * hash_table) {
    const size_t new_capacity = hash_table->capacity << 2;
    Cell ** new_cells = create_hash_table_cells(new_capacity);
    for (size_t i = 0; i < hash_table->capacity; i++) {
        if (full == hash_table->cells[i]->cell_type) {
            Cell * new_cell = find_cell(new_cells, new_capacity, hash_table->cells[i]->key);
            new_cell->key = hash_table->cells[i]->key;
            new_cell->value = hash_table->cells[i]->value;
            new_cell->cell_type = full;
        }
        free(hash_table->cells[i]);
    }
    free(hash_table->cells);
    hash_table->cells = new_cells;
}

HashTable * create_hash_table(void) {
    return create_hash_table_with_capacity(10);
}

HashTable * create_hash_table_with_capacity(const size_t capacity) {
    HashTable * hash_table = malloc(sizeof(HashTable));
    hash_table->capacity = capacity;
    hash_table->size = 0;
    hash_table->cells = create_hash_table_cells(capacity);
    return hash_table;
}

int hash_table_put(HashTable * hash_table, const char * key, const int value) {
    if (hash_table->capacity / (hash_table->size + 1) < 2) {
        resize_hash_table(hash_table);
    }
    Cell * cell = find_cell_in_hash_table(hash_table, key);
    if (full == cell->cell_type) {
        const int prev_value = cell->value;
        cell->value = value;
        return prev_value;
    }
    cell->key = malloc(strlen(key) + 1);
    strcpy(cell->key, key);
    cell->value = value;
    cell->cell_type = full;
    return 0;
}

_Bool hash_table_exists(const HashTable * hash_table, const char * key) {
    const Cell * cell = find_cell_in_hash_table(hash_table, key);
    return full == cell->cell_type;
}

int hash_table_get(const HashTable * hash_table, const char * key) {
    const Cell * cell = find_cell_in_hash_table(hash_table, key);
    return full == cell->cell_type ? cell->value : 0;
}

int hash_table_remove(const HashTable * hash_table, const char * key) {
    Cell * cell = find_cell_in_hash_table(hash_table, key);
    if (full == cell->cell_type) {
        cell->cell_type = empty;
        free(cell->key);
        cell->key = NULL;
        return cell->value;
    }
    return 0;
}

void hash_table_foreach_key_value(HashTable * hash_table, void (*fn)(const char * key, int value)) {
    for (size_t i = 0; i < hash_table->capacity; ++i) {
        if (full == hash_table->cells[i]->cell_type) {
            fn(hash_table->cells[i]->key, hash_table->cells[i]->value);
        }
    }
}

void free_hash_table(HashTable * hash_table) {
    for (size_t i = 0; i < hash_table->capacity; ++i) {
        if (full == hash_table->cells[i]->cell_type) {
            free(hash_table->cells[i]->key);
        }
        free(hash_table->cells[i]);
    }
    free(hash_table->cells);
    free(hash_table);
}
