#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define JPEG_SIGNATURE_SIZE 2

#define JPEG_FIRST_START_BYTE 0xFF
#define JPEG_SECOND_START_BYTE 0xD8
#define JPEG_FIRST_END_BYTE 0xFF
#define JPEG_SECOND_END_BYTE 0xD9

#define ZIP_LOCAL_FILE_SIGNATURE_SIZE 4

#define ZIP_FIRST_START_BYTE 0x50
#define ZIP_SECOND_START_BYTE 0x4B
#define ZIP_THIRD_START_BYTE 0x03
#define ZIP_FOURS_START_BYTE 0x04

#define FILE_NAME_LEN_OFFSET 22L
#define FILE_NAME_LEN_SIZE 2L
#define EXTRA_FIELD_LEN_SIZE 2L

typedef enum listFilesInArchiveResult {
    lfiar_ok = 0,

    lfiar_eof = -1,
    lfiar_errors_in_data = -3,

} ListFilesInArchiveResult;

typedef union twoByteAdapter {
    unsigned char bytes[2];
    unsigned int value;
} TwoByteAdapter;

static const size_t BYTE_SIZE = sizeof(unsigned char);

_Bool seek_bytes(FILE * f, const unsigned char * bytes, size_t n);
ListFilesInArchiveResult list_files_in_arch(FILE * f);
ListFilesInArchiveResult print_local_file_name(FILE * f);

int main(int argc, char * argv[]) {
    if (argc != 2) {
        printf("Usage: %s <file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    int ret_value = EXIT_SUCCESS;
    char * file_name = argv[1];
    FILE * f = fopen(file_name, "rb");
    if (NULL == f) {
        fprintf(stderr, "End: can't open file \"%s\"", file_name);
        ret_value = EXIT_FAILURE;
        goto End;
    }

    unsigned char bytes[2] = {0};
    size_t read = fread(&bytes, BYTE_SIZE, JPEG_SIGNATURE_SIZE, f);
    if (JPEG_SIGNATURE_SIZE != read) {
        fprintf(stderr, "End: can't jpeg start signature \"%s\"", file_name);
        ret_value = EXIT_FAILURE;
        goto End;
    }
    if (bytes[0] != JPEG_FIRST_START_BYTE || bytes[1] != JPEG_SECOND_START_BYTE) {
        fprintf(stderr, "End: wrong file type \"%s\"", file_name);
        ret_value = EXIT_FAILURE;
        goto End;
    }
    unsigned char jpeg_end_signature[] = {JPEG_FIRST_END_BYTE, JPEG_SECOND_END_BYTE};
    _Bool seek_result = seek_bytes(f, jpeg_end_signature, JPEG_SIGNATURE_SIZE);
    if (!seek_result) {
        fprintf(stderr, "End: can't find jpeg end bytes in file \"%s\"", file_name);
        ret_value = EXIT_FAILURE;
        goto End;
    }
    int list_result = list_files_in_arch(f);
    switch (list_result) {
        case lfiar_ok:
            break;
        case lfiar_eof:
            printf("No archive at the end of \"%s\"", file_name);
            ret_value = EXIT_SUCCESS;
            goto End;
        case lfiar_errors_in_data:
            printf("Error: errors in archive data \"%s\"", file_name);
            ret_value = EXIT_FAILURE;
            goto End;
        default:
            fprintf(stderr, "!!!Unhandled result case of call function list_files_in_arch!!!");
            break;
    }

    End:
    if (f) {
        fclose(f);
    }
    return ret_value;
}

_Bool seek_bytes(FILE * f, const unsigned char * bytes, size_t n) {
    unsigned char byte = 0;
    _Bool result = false;
    while (1 == fread(&byte, BYTE_SIZE, 1, f)) {
        if (bytes[0] == byte) {
            result = true;
            off_t cur_pos = ftello(f);
            for (size_t i = 1; i < n; ++i) {
                if (1 != fread(&byte, BYTE_SIZE, 1, f) || bytes[i] != byte) {
                    result = false;
                    break;
                }
            }
            if (result) {
                break;
            }
            fseeko(f, cur_pos, SEEK_SET);
        }
    }
    return result;
}

ListFilesInArchiveResult list_files_in_arch(FILE * f) {
    int eof = feof(f);
    if (eof) {
        return lfiar_eof;
    }
    ListFilesInArchiveResult print_result = lfiar_ok;
    unsigned char zip_local_file_signature[] = {
            ZIP_FIRST_START_BYTE,
            ZIP_SECOND_START_BYTE,
            ZIP_THIRD_START_BYTE,
            ZIP_FOURS_START_BYTE
    };
    while (seek_bytes(f, zip_local_file_signature, ZIP_LOCAL_FILE_SIGNATURE_SIZE)) {
        print_result = print_local_file_name(f);
        if (lfiar_ok != print_result) {
            break;
        }
    }
    return print_result;
}

ListFilesInArchiveResult print_local_file_name(FILE * f) {
    int seek_result = fseek(f, FILE_NAME_LEN_OFFSET, SEEK_CUR);
    if (seek_result) {
        return lfiar_errors_in_data;
    }
    TwoByteAdapter file_name_size = {.value = 0};
    size_t read = fread(file_name_size.bytes, BYTE_SIZE, FILE_NAME_LEN_SIZE, f);
    if (FILE_NAME_LEN_SIZE != read) {
        return lfiar_errors_in_data;
    }
    seek_result = fseek(f, EXTRA_FIELD_LEN_SIZE, SEEK_CUR);
    if (seek_result) {
        return lfiar_errors_in_data;
    }
    char * file_name = malloc(sizeof(char) * file_name_size.value + 1);
    file_name[file_name_size.value] = '\0';
    read = fread(file_name, sizeof(char), file_name_size.value, f);
    if (read != file_name_size.value) {
        return lfiar_errors_in_data;
    }
    printf("%s\n", file_name);
    free(file_name);
    return lfiar_ok;
}
