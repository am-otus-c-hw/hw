#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <curl/curl.h>

#include <cjson/cJSON.h>

#define API_URL_TEMPLATE "https://wttr.in/%s?format=j1"
#define API_URL_SIZE 25

typedef struct {
    size_t size;
    char * data;
} Memory;

static size_t write_func(void * contents, size_t size, size_t nmemb, void  *userp) {
    size_t real_size = size * nmemb;
    Memory * memory = (Memory *)userp;

    char * ptr = realloc(memory->data, memory->size + real_size + 1);
    if (NULL == ptr) {
        fprintf(stderr, "Error: can't realloc memory");
        return 0;
    }

    memory->data = ptr;
    memcpy(&(memory->data[memory->size]), contents, real_size);
    memory->size += real_size;
    memory->data[memory->size] = '\0';

    return real_size;
}

_Bool print_weather(cJSON * response_json) {
    const cJSON * current_condition = cJSON_GetObjectItem(response_json, "current_condition");
    if (NULL == current_condition) {
        fprintf(stderr, "Error: unknown json format");
        return false;
    }

    cJSON * current_weather = cJSON_GetArrayItem(current_condition, 0);
    if (NULL == current_weather) {
        fprintf(stderr, "Error: unknown json format");
        return false;
    }

    cJSON * current_temp = cJSON_GetObjectItem(current_weather, "temp_C");
    if (NULL == current_temp) {
        fprintf(stderr, "Error: no field \"temp_C\"");
        return false;
    }

    cJSON * wind_direction = cJSON_GetObjectItem(current_weather, "winddir16Point");
    if (NULL == wind_direction) {
        fprintf(stderr, "Error: no field \"winddir16Point\"");
        return false;
    }

    cJSON * wind_speed = cJSON_GetObjectItem(current_weather, "windspeedKmph");
    if (NULL == wind_speed) {
        fprintf(stderr, "Error: no field \"windspeedKmph\"");
        return false;
    }

    const cJSON * weather = cJSON_GetObjectItem(response_json, "weather");
    if (NULL == weather) {
        fprintf(stderr, "Error: unknown json format");
        return false;
    }

    cJSON * current_day = cJSON_GetArrayItem(weather, 0);
    if (NULL == current_weather) {
        fprintf(stderr, "Error: unknown json format");
        return false;
    }

    cJSON * min_day_temp = cJSON_GetObjectItem(current_day, "mintempC");
    if (NULL == min_day_temp) {
        fprintf(stderr, "Error: no field \"mintempC\"");
        return false;
    }

    cJSON * max_day_temp = cJSON_GetObjectItem(current_day, "maxtempC");
    if (NULL == max_day_temp) {
        fprintf(stderr, "Error: no field \"maxtempC\"");
        return false;
    }

    printf(
            "Weather: current temperature %s C, current wind %s %s Kmph. Day min temperature %s C, max temperature %s C.",
            cJSON_GetStringValue(current_temp),
            cJSON_GetStringValue(wind_direction),
            cJSON_GetStringValue(wind_speed),
            cJSON_GetStringValue(min_day_temp),
            cJSON_GetStringValue(max_day_temp)
    );
    return true;
}

int main(int argc, char * argv[]) {
    if (2 != argc) {
        printf("Usage: %s <location_name> ", argv[0]);
        return EXIT_FAILURE;
    }

    CURL * curl = curl_easy_init();
    if (NULL == curl) {
        fprintf(stderr, "Error: can't init curl lib");
        return EXIT_FAILURE;
    }

    CURLcode code;
    char * api_url = malloc(sizeof(char) * (API_URL_SIZE + strlen(argv[1]) + 1));
    if (NULL == api_url) {
        fprintf(stderr, "Error: can't allocate memory");
        goto End;
    }

    Memory chunk = {.size = 0, .data = malloc(1)};
    if (NULL == chunk.data) {
        fprintf(stderr, "Error: can't allocate memory");
        goto End;
    }

    sprintf(api_url, API_URL_TEMPLATE, argv[1]);
    curl_easy_setopt(curl, CURLOPT_URL, api_url);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_func);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
    code = curl_easy_perform(curl);
    if (CURLE_OK != code) {
        fprintf(stderr, "Error: can't execute query");
        goto End;
    }

    long response_code;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
    if (200 != response_code) {
        fprintf(stderr, "Error: service response code %ld", response_code);
        goto End;
    }

    cJSON * response_json = cJSON_Parse(chunk.data);
    if (NULL == response_json) {
        fprintf(stderr, "Error: can't parse response \"%s\"", chunk.data);
        goto End;
    }

    if (!print_weather(response_json)) {
        fprintf(stderr, "Error: can't load current weather from \"%s\"", chunk.data);
    }

    if (response_json) {
        cJSON_Delete(response_json);
    }
    End:
    if (chunk.data) {
        free(chunk.data);
    }
    if (api_url) {
        free(api_url);
    }
    if (curl) {
        curl_easy_cleanup(curl);
    }

    return EXIT_SUCCESS;
}
