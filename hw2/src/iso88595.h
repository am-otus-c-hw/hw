#ifndef ISO88595_H_
#define ISO88595_H_

void iso88595_to_utf8(unsigned char symbol, unsigned char * utf8_bytes);

#endif // ISO88595_H_
