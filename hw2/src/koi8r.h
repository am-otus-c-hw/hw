#ifndef KOI8R_H_
#define KOI8R_H_

void koi8r_to_utf8(unsigned char symbol, unsigned char * utf8_bytes);

#endif // KOI8R_H_
