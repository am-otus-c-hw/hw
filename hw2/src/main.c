#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "koi8r.h"
#include "cp1251.h"
#include "iso88595.h"

#define CP1251 "cp-1251"
#define KOI8R "koi8-r"
#define ISO88595 "iso-8859-5"

static const size_t BYTE_SIZE = sizeof(unsigned char);

int main(int argc, char * argv[]) {
    if (4 != argc) {
        printf("Usage: %s <in file> <code page[koi8-r,cp-1251,iso-8859-5]> <out file>\n", argv[0]);
        return EXIT_FAILURE;
    }
    int ret_value = EXIT_SUCCESS;
    const char * in_file_name = argv[1];
    FILE * in_f = fopen(in_file_name, "rb");
    if (NULL == in_f) {
        fprintf(stderr, "End: can't open in file \"%s\"", in_file_name);
        return EXIT_FAILURE;
    }

    const char * out_file_name = argv[3];
    FILE * out_f = fopen(out_file_name, "wb+");
    if (NULL == out_f) {
        fprintf(stderr, "End: can't open out file \"%s\"", out_file_name);
        ret_value = EXIT_FAILURE;
        goto End;
    }


    void (*convert_function)(unsigned char symbol, unsigned char * utf8_bytes) = NULL;
    const char * code_page = argv[2];
    if (0 == strcmp(code_page, KOI8R)) {
        convert_function = koi8r_to_utf8;
    } else if (0 == strcmp(code_page, CP1251)) {
        convert_function = cp1251_to_utf8;
    } else if (0 == strcmp(code_page, ISO88595)) {
        convert_function = iso88595_to_utf8;
    } else {
        fprintf(stderr, "End: Unknown code page \"%s\", use one of koi8-r, cp-1251, iso-8859-5", code_page);
        ret_value = EXIT_FAILURE;
        goto End;
    }

    unsigned char current_byte = '\0';
    unsigned char utf8_bytes[2] = {0};
    size_t bytes_to_write;
    while (true) {
        if (1 != fread(&current_byte, BYTE_SIZE, 1, in_f)) {
            if (!feof(in_f)) {
                fprintf(stderr, "End: can't read in file \"%s\"", in_file_name);
                ret_value = EXIT_FAILURE;
            }
            goto End;
        }
        if (0x00 <= current_byte && 0x7f >= current_byte) {
            bytes_to_write = 1;
            utf8_bytes[0] = current_byte;
        }
        else {
            bytes_to_write = 2;
            convert_function(current_byte, utf8_bytes);
        }
        if (bytes_to_write != fwrite(utf8_bytes, BYTE_SIZE, bytes_to_write, out_f)) {
            fprintf(stderr, "End: can't write out file \"%s\"", out_file_name);
            ret_value = EXIT_FAILURE;
            goto End;
        }
    }

    End:
    if (out_f) {
        fclose(out_f);
    }
    if (in_f) {
        fclose(in_f);
    }
    return ret_value;
}
