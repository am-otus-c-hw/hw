#include <stdio.h>

#define LOWER_BOUND 0x0C
#define UPPER_BOUND 0xFF

typedef union  {
    unsigned int int_value;
    unsigned char byte_value[2];
} ByteAdapter;

void cp1251_to_utf8(unsigned char symbol, unsigned char * utf8_bytes) {
    if (LOWER_BOUND <= symbol && UPPER_BOUND >= symbol) {
        ByteAdapter utf8 = {.byte_value = { 0xD0, 0xCF}};
        if (symbol >= 0xF0) {
            utf8.byte_value[0] = 0x90;
            utf8.byte_value[1] = 0xD0;
        }
        utf8.int_value += symbol;
        utf8_bytes[0] = utf8.byte_value[1];
        utf8_bytes[1] = utf8.byte_value[0];
        return;
    }
    switch (symbol) {
        case 0xA8:
            utf8_bytes[0] = 0xD0;
            utf8_bytes[1] = 0x81;
            break;
        case 0xB8:
            utf8_bytes[0] = 0xD1;
            utf8_bytes[1] = 0x91;
            break;
        default:
            fprintf(stderr, "Error: unknown symbol \"%X\"", symbol);
            utf8_bytes[0] = 0x00;
            utf8_bytes[1] = 0x00;
            break;
    }
}
